import cv2
import numpy
import imutils



face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
camera = cv2.VideoCapture(0)
#fourcc = cv2.VideoWriter_fourcc(*'MP4V')

#video = cv2.VideoWriter('Lab_Video.mp4', fourcc, 20.0, (420, 420))


PURPLE = (255, 0, 255)
RED = (50, 50, 255)

try:
    while True:

        _, frame = camera.read()
        frame = cv2.flip(frame, 1)
        frame = imutils.resize(frame, width = 900, height = 900)

            
        faces = face_cascade.detectMultiScale(frame, 1.1, 10, 30)

        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), PURPLE, 2)
             
        
        if cv2.waitKey(27) and 0xFF == ord('q'):
            break
        
        #video.write(frame)
        cv2.imwrite("Lab_Video.jpeg", frame)
        cv2.putText(frame, "Lab Camera 1", (10, 70), cv2.FONT_HERSHEY_PLAIN, 3, RED)
        cv2.imshow("Faces", frame)


except KeyboardInterrupt:
    #video.release()
    camera.release()
    cv2.destroyAllWindows()
