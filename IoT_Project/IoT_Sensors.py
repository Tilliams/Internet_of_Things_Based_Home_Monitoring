import grovepi, grove_rgb_lcd, time
from threading import Thread
import sqlite3

#Setup Port Numbers

#Analog Ports
SOUND = 0
LIGHT = 1


#Digital Ports
MOTION = 7

WEATHER = 8

#Set up Colors
#LCD is RGB
#RED is BGR for OpenCV


grove_rgb_lcd.setRGB(255, 0, 255)


#Setup Reading Objects

lights = grovepi.analogRead(LIGHT)
sounds = grovepi.analogRead(SOUND)

#Set up Functions To read Data From Sensors
def read_weather():
    [temp, hum] = grovepi.dht(WEATHER, 0)
    temp = 1.8*temp + 32

   
    return 'Temperature:%.2f F, Humidity: %.0f%s'%(temp, hum, "%") 

mins = 1
mins = 60 * mins

def main():
	while True:
		#file = open('Sensors.txt', 'w')
		#Read Light intensity
		lights = grovepi.analogRead(LIGHT)
		if lights > 100:
			light_message = "Light's are On!!"
		else:
			light_message = "Light's are Off!!"
		
		
		#Read Sound Intensity
		sounds = grovepi.analogRead(SOUND)
		if sounds > 100:
			sound_message = "Someone's speaking!!!"
		else:
			sound_message = "It's Silent!!!"
			
		far = grovepi.ultrasonicRead(MOTION)
		
		far_message = "Someone's {} cm away ".format(far)
		
		
		
		
		grove_rgb_lcd.setText(read_weather())
		
		weather_message = read_weather()
		
		
		time.sleep(5)
		
		message = " {}\n {}\n {}\n {}\n".format(light_message, sound_message, far_message, weather_message)
		
		print(message)
		
		#write(message, 'Sensors.txt')
		
def write(m, f):
	f = open(f, 'w')
	f.write(m)
	f.close()
		
			

##try:
##    main()
##    
##        
##
##except KeyboardInterrupt:
##    print("System off")
##    grove_rgb_lcd.setText("System Off")
##    file.close()

        
            

main()
