#!\bin\bash

#Installing GrovePi Library
sudo curl -kL dexterindustries.com/update_grovepi | bash


## OPENCV INSTALLATION FOR RASPBERRY PI 
## These Instructions may also be found at 
## https://www.pyimagesearch.com/2016/04/18/install-guide-raspberry-pi-3-raspbian-jessie-opencv-3/ 

sudo apt-get purge wolfram-engine
sudo apt-get update
sudo apt-get upgrade

sudo apt-get install -y build-essential cmake pkg-config
sudo apt-get install -y libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install -y libxvidcore-dev libx264-dev

sudo apt-get install -y libgtk2.0-dev
sudo apt-get install -y libatlas-base-dev gfortran

sudo apt-get install -y python3-dev

cd ~
wget -O opencv.zip https://github.com/Itseez/opencv/archive/3.4.0.zip
unzip opencv.zip

wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.4.0.zip
unzip opencv_contrib.zip


wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py



sudo pip3 install numpy

cd ~/opencv-3.4.0/

mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=RELEASE
cmake -D CMAKE_INSTALL_PREFIX=/usr/local
cmake -D INSTALL_PYTHON_EXAMPLES=ON
cmake -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-3.4.0/modules
cmake -D BUILD_EXAMPLES=ON ..


make -j4
make clean

sudo make install
sudo ldconfig

cd /usr/local/lib/python3.5/dist-packages/

sudo mv cv2.cpython-35m-arm-linux-gnueabihf.so cv2.so

ln -s /usr/local/lib/python3.5/site-packages/cv2.so cv2.so
